/*

Activity Instructions:
1. Create a User schema with the following fields: firstName, lastName, username, password, email.
2. Create a Product schema with the following fields: name, description, price.
3. Create a User Model and  a Product Model.
4. Create a POST request that will access the /register route which will create a user. Test this in Postman app.
5. Create a POST request that will access the /createProduct route which will create a product. Test this in Postman app.
6. Create a GET request that will access the /users route to retrieve all users from your DB. Test this in Postman.
7. Create a GET request that will access the /products route to retrieve all products from your DB. Test this in Postman.

Note: create a separate Database collection named S35-C1 in your MongoDb account.
*/

const express = require ("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

mongoose.connect('mongodb+srv://gabrieldguillermo:admin123@zuitt-batch-197.w7cnjly.mongodb.net/S35-C1?retryWrites=true&w=majority',{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection

db.on('error', console.error.bind(console,'connection'));
db.on('open',() => console.log('Connection to mongodb!'));

// User Schema
const userSchema = new mongoose.Schema({
	firstName : String,
	lastName : String, 
	username : String, 
	password : String, 
	email : String
});

// Product Schema
const productSchema = new mongoose.Schema({
	 name : String,
	 description : String,
	 price : Number
})


// User model
const User = mongoose.model("User", userSchema);
// Product model
const Product = mongoose.model("Product", productSchema);

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));


/*========== POST ===========*/

// user
app.post('/register', (req, res) => {
	User.findOne({
		username : req.body.username
	
	}, (error, result) => {

		if(error){
			return res.send(error)
		} else if (result != null && result.username ==  req.body.username ){
			return res.send(`Username "${req.body.username}" is already taken!`)
		} else {
			let newUser = new User({
				firstName: req.body.firstName,
				lastName : req.body.lastName, 
				username : req.body.username, 
				password :req.body.password, 
				email : req.body.email
			
			});

			newUser.save((error, savedUser)=> {
				if(error){
					return console.error(error)
				} else {
					return res.status(201).send(`New User Created! ${savedUser}`)
				}
			});

		}
	})

});

// product
app.post('/create-product',(req, res) => {
	Product.findOne({
		name:req.body.name
	},(error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.name == req.body.name) {
			return res.send(`Product name "${req.body.name}" is already been use!`)
		} else {
			let newProduct = new Product({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price
			});

			newProduct.save((error, savedProduct) => {
				if(error){
					return console.log(error);
				} else {
					return res.status(201).send(`New product Created! ${savedProduct}` );
					
				}
			})

		}
	}) 
});




/*========== GET ===========*/

// all users
app.get('/users',(req, res) => {
	User.find({},(error, result)=>{
		return error ? res.send(error): res.status(200).json({ User: result})
	});
});


app.get('/products',(req, res) => {
	Product.find({}, (error, result) => {
		return error ? res.send(error): res.status(200).json({ Product: result})

	})
})




app.listen(port, () => console.log(`Server is running at port: ${port}`));